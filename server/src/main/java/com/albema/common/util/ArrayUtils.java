package com.albema.common.util;

import java.util.Arrays;

/**
 * Created by alexandremasanes on 09/04/2018.
 */
public class ArrayUtils {

    public static <T> T[] unshift(T[] array, T value) {
        array = Arrays.copyOfRange(array, -1, array.length);
        array[0] = value;
        return array;
    }

    private ArrayUtils() throws IllegalAccessException {
        throw new IllegalAccessException();
    }
}
