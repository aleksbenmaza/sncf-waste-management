package com.albema.common.util;

import com.albema.spring.validation.UniqueEntityProperty;
import org.apache.commons.lang.ArrayUtils;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
public class AnnotationUtils {

    private final static String FIELD_NOT_SUPPORTED_FORMAT;

    static {
        FIELD_NOT_SUPPORTED_FORMAT = "Annotation %s does not target field !";
    }

    public static Field getField(Class clazz, Annotation annotation)  {

        if(
                annotation.getClass()
                          .isAnnotationPresent(Target.class)
             && !ArrayUtils.contains(
                     annotation.getClass()
                               .getAnnotation(Target.class)
                               .value(),
                     ElementType.FIELD
                )
        )
            throw new IllegalArgumentException(
                    String.format(
                            FIELD_NOT_SUPPORTED_FORMAT,
                            annotation.getClass()
                                      .getName()));

        for(Field field : clazz.getDeclaredFields())
            if(
                    field.isAnnotationPresent(UniqueEntityProperty.class)
                 && annotation == field.getAnnotation(annotation.getClass())
            ) {
                return field;
            }

        return null;
    }

    AnnotationUtils() {}
}
