package com.albema.common.util;


import java.math.BigInteger;

import static java.lang.Math.log10;

/**
 * Created by alexandremasanes on 24/08/2017.
 */
public final class MathUtils {

    public static long digitCount(long number) {
        return number == 0 ? 1 : (long) log10(number) + 1;
    }

    public static long digitCount(BigInteger number) {
        long count;
        BigInteger[] resultAndRemainder;

        if(number.equals(BigInteger.ZERO))
            return 1;
        count = 0;
        do {
            resultAndRemainder = number.divideAndRemainder(BigInteger.TEN);
            number = resultAndRemainder[0];
            count++;
        } while (number.compareTo(BigInteger.ZERO) != 0);

        return count;
    }

    MathUtils() {}
}
