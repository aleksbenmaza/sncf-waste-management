package com.albema.common.orm.repository;

import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;

import java.util.List;

/**
 * Created by alexandremasanes on 21/02/2017.
 */
public interface Finder<T extends IdentifiedByIdEntity> {

    List<T> find();

    List<T> find(long... ids);

    T       find(long id);

    boolean has(long id);

    long    getNextId();
}