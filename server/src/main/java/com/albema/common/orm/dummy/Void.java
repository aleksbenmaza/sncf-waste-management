package com.albema.common.orm.dummy;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by alexandremasanes on 27/03/2018.
 */
@Entity
@Immutable
@Table(name = "void")
public class Void {

    @Id
    private Boolean value;
}
