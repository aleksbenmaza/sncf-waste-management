package com.albema.common.orm.repository;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;

;

/**
 * Created by alexandremasanes on 21/02/2017.
 */
public interface Remover<T extends RecordableEntity & IdentifiableById> {

    boolean remove(T entity);

    boolean remove(T... entities);

    boolean remove(long... ids);

    boolean remove(long id);
}
