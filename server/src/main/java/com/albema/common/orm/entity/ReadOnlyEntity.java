package com.albema.common.orm.entity;

import org.hibernate.annotations.Immutable;

/**
 * Created by alexandremasanes on 30/01/2018.
 */
@Immutable
public abstract class ReadOnlyEntity extends BaseEntity {

    private static final long serialVersionUID = -657784067750896549L;
}
