package com.albema.spring.repository;


import com.albema.common.orm.entity.BaseEntity;
import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;
import com.alda.sncf.wastemanagement.business.mapping.entity.Company;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Inheritance;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.asList;

@SuppressWarnings("unchecked")
public abstract class DAO {

    private final SessionFactory sessionFactory;

    protected DAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected final Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    protected <E extends BaseEntity> List<E> find(Class<E> entityClass) {
        Query<E> query;
        query = getCurrentSession().createQuery(" FROM " + entityClass.getName());

        return query.list();
    }

    protected <E extends IdentifiedByIdEntity> List<E> find(Class<E> entityClass, long... ids) {
        Query<E> query;
        query = getCurrentSession().createQuery("FROM " + entityClass.getName() + " c WHERE c.id IN :ids");
        query.setParameterList("ids", asList(ids));

        return query.list();
    }

    protected <E extends IdentifiedByIdEntity> E find(Class<E> entityClass, long id) {
        Query<E> query;
        query = getCurrentSession().createQuery("FROM " + entityClass.getName() + " c WHERE c.id = :id");
        query.setParameter("id", id);

        return query.uniqueResult();
    }

    protected <E extends BaseEntity, T> List<E> find(Class<E> entityClass, T[] tuples, String[] attributes, Function<T, Serializable>[] getters) {
        final String stm;
        final Query<E> query;

        if(getters.length + attributes.length == 0 || getters.length != attributes.length)
            throw new IllegalArgumentException();

        stm = "FROM " + entityClass.getName() + " WHERE " + createCompositeInClause(tuples.length, attributes);

        query = getCurrentSession().createQuery(stm);

        bindCompositeInClause(query, tuples, attributes, getters);

        return query.list();
    }

    protected void save0(RecordableEntity entity) {
        final Session session;

        session = getCurrentSession();

        session.saveOrUpdate(entity);

        session.flush();
    }

    protected boolean remove(Class<? extends IdentifiedByIdEntity> entityClass, long... ids) {
        String stm;

        if (ids.length == 0)
            return false;

        stm = "DELETE FROM " + entityClass.getName() + " c WHERE c.id IN :ids";

        return getCurrentSession().createQuery(stm)
                                  .setParameter("ids", Arrays.asList(ids))
                                  .executeUpdate() != 0;
    }

    protected <E extends BaseEntity, T> boolean remove(Class<E> entityClass, T[] tuples, String[] attributes, Function<T, Serializable>[] getters) {
        final String stm;
        final Query<E> query;

        if(getters.length + attributes.length == 0 || getters.length != attributes.length)
            throw new IllegalArgumentException();

        stm = "DELETE FROM " + entityClass.getName() + " WHERE " + createCompositeInClause(tuples.length, attributes);

        query = getCurrentSession().createQuery(stm);

        bindCompositeInClause(query, tuples, attributes, getters);

        return query.executeUpdate() != 0;
    }

    protected boolean remove0(RecordableEntity entity) {
        if (!getCurrentSession().contains(entity))
            return false;
        getCurrentSession().delete(entity);
        return true;
    }

    protected boolean remove0(RecordableEntity... entities) {

        String stm;

        Class<? extends RecordableEntity> entityClass;

        if (entities.length == 0)
            return false;

        entityClass = entities[0].getClass();

        for(RecordableEntity entity : entities)
            if(entityClass != entity.getClass())
                throw new IllegalArgumentException();

        stm = "DELETE FROM " + Company.class.getName() + " e "
            + "WHERE e IN :entities";

        return getCurrentSession().createQuery(stm)
                                  .setParameter("entities", Arrays.asList(entities))
                                  .executeUpdate() != 0;
    }

    /**
     * TODO finish
     */
    protected static Class resolveCommonAncestorEntityClass(BaseEntity... entities){
        Class ancestorEntityClass;

        ancestorEntityClass = entities[0].getClass().getSuperclass();

        do {
            for(BaseEntity entity : entities)
                if(ancestorEntityClass == entity.getClass().getSuperclass())
                    ancestorEntityClass = entity.getClass().getSuperclass();

        } while(ancestorEntityClass.getAnnotation(Inheritance.class) == null);

        return ancestorEntityClass;
    }

    private <E extends BaseEntity, T> void bindCompositeInClause(Query<E> query, T[] tuples, String[] attributes, Function<T, Serializable>[] getters) {
        int i;
        int j;

        for(i = 0; i < tuples.length; ++i)
            for(j = 0; j < attributes.length; ++j)
                query.setParameter(attributes[j] + i, getters[j].apply(tuples[i]));
    }

    private <T> String createCompositeInClause(int size, String[] attributes) {
        final String orStmFormat;

        final ArrayList<String> orStms;

        int i;

        if(attributes.length == 0)
            throw new IllegalArgumentException();

        orStmFormat = StringUtils.join(attributes, "%1$i AND ") + "%1$i";

        orStms = new ArrayList<>();

        for(i = 0; i < size; ++i)
            orStms.add(String.format(orStmFormat, i));

        return '(' + StringUtils.join(orStms, " OR ") + ')';
    }
}