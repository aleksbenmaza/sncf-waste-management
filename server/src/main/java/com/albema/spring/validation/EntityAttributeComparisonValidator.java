package com.albema.spring.validation;

import org.hibernate.SessionFactory;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

/**
 * Created by alexandremasanes on 10/04/2018.
 */
public class EntityAttributeComparisonValidator extends BusinessDataValidator<LessOrEqualEntityProperty> {

   EntityAttributeComparisonValidator(SessionFactory sessionFactory) {
      super(sessionFactory);
   }

   public boolean isValid(Serializable obj, ConstraintValidatorContext context) {
      return false;
   }
}
