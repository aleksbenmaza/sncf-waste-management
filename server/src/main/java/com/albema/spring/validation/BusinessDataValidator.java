package com.albema.spring.validation;

import com.albema.common.util.AnnotationUtils;
import com.albema.common.util.ObjectUtils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.validation.ConstraintValidator;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.function.Supplier;

/**
 * Created by alexandremasanes on 27/03/2018.
 */
public abstract class BusinessDataValidator<C extends Annotation> implements ConstraintValidator<C, Serializable> {

    private C constraintAnnotation;

    private final SessionFactory sessionFactory;

    @Override
    public void initialize(C constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    protected C getConstraintAnnotation() {
        return this.constraintAnnotation;
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    protected String resolveFieldName(Serializable o, Supplier<String> supplier) {
        String entityProperty;
        entityProperty = supplier.get();

        if("".equals(entityProperty))
            entityProperty = ObjectUtils.ifNotNull(
                    AnnotationUtils.getField(
                            o.getClass(),
                            getConstraintAnnotation()),
                    Field::getName,
                    null
            );
        return entityProperty;
    }

    BusinessDataValidator(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}