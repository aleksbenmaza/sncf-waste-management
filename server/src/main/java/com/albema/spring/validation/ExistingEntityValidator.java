package com.albema.spring.validation;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
public class ExistingEntityValidator extends BusinessDataValidator<ExistingEntity> {

    public ExistingEntityValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isValid(
            Serializable o,
            ConstraintValidatorContext constraintValidatorContext
    ) {
        String stm;

        stm = "SELECT EXISTS("
            +     "FROM " + getConstraintAnnotation().value() + " "
            +     "WHERE id=:value"
            + ")";

        return (boolean) getCurrentSession().createQuery(stm)
                                            .setParameter("value", o)
                                            .uniqueResult();
    }
}
