package com.albema.spring.validation;

import com.albema.common.orm.entity.BaseEntity;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by alexandremasanes on 10/04/2018.
 */
@Constraint(validatedBy = EntityAttributeComparisonValidator.class)
@Target({
        ElementType.FIELD,
        ElementType.PARAMETER
})
@Retention(RetentionPolicy.RUNTIME)
public @interface LessOrEqualEntityProperty {

    Class<? extends BaseEntity> entityClass();

    String entityAttribute() default "";
}
