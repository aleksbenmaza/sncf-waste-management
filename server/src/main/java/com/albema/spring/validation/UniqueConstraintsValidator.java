package com.albema.spring.validation;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

/**
 * Created by alexandremasanes on 27/03/2018.
 */
@Component
class UniqueConstraintsValidator extends BusinessDataValidator<UniqueConstraints> {

    UniqueConstraintsValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public boolean isValid(Serializable serializable, ConstraintValidatorContext constraintValidatorContext) {
       return false;
    }

}
