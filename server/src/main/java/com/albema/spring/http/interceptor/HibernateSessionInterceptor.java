package com.albema.spring.http.interceptor;


import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import static com.albema.common.util.ObjectUtils.doIf;

/**
 * Created by alexandremasanes on 09/10/2017.
 */
@Component
public class HibernateSessionInterceptor extends HandlerInterceptorAdapter {

    private final static Logger logger;

    static {
        logger = LoggerFactory.getLogger(HibernateSessionInterceptor.class);
    }

    private final HashMap<HttpServletRequest, EntityManager> sessionsRequests;

    private final SessionFactory sessionFactory;

    {
        sessionsRequests = new HashMap<>();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        EntityManager session;
        HttpSession httpSession;
        session = sessionFactory.createEntityManager();

        sessionsRequests.put(request, session);

        httpSession = request.getSession();

        if(httpSession != null)
            extractEntities(httpSession).forEach((attributeName, entity) ->
                    httpSession.setAttribute(
                            attributeName,
                            session.find(
                                    entity.getClass(),
                                    entity.getId()
                            )
                    )
            );

        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(
            HttpServletRequest  request,
            HttpServletResponse response,
            Object              handler,
            ModelAndView        modelAndView
    ) throws Exception {
        if(request.getSession() != null)
            extractEntities(
                    request.getSession()
            ).values()
             .forEach(
                     sessionsRequests.get(request)::detach
             );
        sessionsRequests.remove(request);
        super.postHandle(request, response, handler, modelAndView);
    }

    private Map<String, IdentifiedByIdEntity> extractEntities(HttpSession httpSession) {
        HashMap<String, IdentifiedByIdEntity> entities;
        Enumeration<String>  sessionAttributeNames;
        String sessionAttributeName;
        Object sessionAttribute;

        entities = new HashMap<>();

        sessionAttributeNames = httpSession.getAttributeNames();

        while(sessionAttributeNames.hasMoreElements()) {
            sessionAttributeName = sessionAttributeNames.nextElement();
            sessionAttribute     = httpSession.getAttribute(sessionAttributeName);
            if(sessionAttribute instanceof IdentifiedByIdEntity)
                entities.put(sessionAttributeName, (IdentifiedByIdEntity) sessionAttribute);
        }
        return entities;
    }

    protected HibernateSessionInterceptor(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @PreDestroy
    protected void destroyed() {
        sessionsRequests.values()
                        .stream()
                        .filter(EntityManager::isOpen)
                        .forEach(EntityManager::close);

        doIf(sessionFactory::close, sessionFactory.isOpen());
    }
}