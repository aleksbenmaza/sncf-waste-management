package com.alda.sncf.wastemanagement.web.api.http.error;

import com.albema.common.http.error.CustomHttpExceptions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alexandremasanes on 29/03/2018.
 */
@ControllerAdvice(annotations = RestController.class)
public class ErrorHandler {

    @ExceptionHandler(CustomHttpExceptions.CommandNotValidatedException.class)
    public ResponseEntity<?> onError(CustomHttpExceptions.CommandNotValidatedException exception) {

        HttpStatus httpStatus;

        exception.printStackTrace();

        if(exception.getClass().isAnnotationPresent(ResponseStatus.class)) {
            httpStatus = exception.getClass().getAnnotation(ResponseStatus.class).value();
            return new ResponseEntity<>((exception).getErrors() , httpStatus);
        }
        else
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        return new ResponseEntity<>(httpStatus);
    }

    @ExceptionHandler(CustomHttpExceptions.HttpException.class)
    public ResponseEntity<Void> onError(CustomHttpExceptions.HttpException exception) {

        HttpStatus httpStatus;

        exception.printStackTrace();

        if(exception.getClass().isAnnotationPresent(ResponseStatus.class))
            httpStatus = exception.getClass().getAnnotation(ResponseStatus.class).value();
        else
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        return new ResponseEntity<>(httpStatus);
    }
}