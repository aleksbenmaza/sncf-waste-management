package com.alda.sncf.wastemanagement.web.api.model.output;


import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
public class StockDTO implements Serializable {

    @SerializedName("material")
    private MaterialDTO materialDTO;

    private Stock.Quality quality;

    private BigDecimal quantity;

    private HashMap<Long, BigDecimal> projectIdQuantities;

    private BigDecimal usedQuantity;

    {
        projectIdQuantities = new HashMap<>();
    }

    public StockDTO(Stock stock) {
        materialDTO = new MaterialDTO(stock.getMaterial());

        quality      = stock.getQuality();
        quantity     = stock.getQuantity();
        usedQuantity = stock.getUsedQuantity();

        stock.getCompositions().forEach(
                c -> projectIdQuantities.put(c.getProject().getId(), c.getQuantity())
        );
    }

    public static List<StockDTO> fromCollection(Collection<Stock> stocks) {
        ArrayList<StockDTO> stockDTOs;

        stockDTOs = new ArrayList<>();

        for(Stock stock : stocks)
            stockDTOs.add(new StockDTO(stock));

        return stockDTOs;
    }
}