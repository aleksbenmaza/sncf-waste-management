package com.alda.sncf.wastemanagement.web.api.http.controller.thirdparty;

import com.alda.sncf.wastemanagement.business.logic.CompanyService;
import com.alda.sncf.wastemanagement.web.api.model.output.CompanyDTO;
import com.alda.sncf.wastemanagement.web.api.model.output.DTO;

import com.alda.sncf.wastemanagement.web.common.business.helper.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("thirdPartyCompanyController")
@RequestMapping("/tp/companies")
public class CompanyController extends Controller {

    private final CompanyService companyService;

    public CompanyController(TokenManager tokenManager, CompanyService companyService) {
        super(tokenManager);
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyDTO> onGet() {
        return DTO.fromCollection(companyService.getAll(), CompanyDTO::new);
    }

    @GetMapping("/{id}")
    public CompanyDTO onGet(@PathVariable long id) {
        return new CompanyDTO(companyService.getOne(id));
    }
}
