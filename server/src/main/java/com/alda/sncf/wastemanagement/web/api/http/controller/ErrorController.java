package com.alda.sncf.wastemanagement.web.api.http.controller;

import com.albema.common.http.error.CustomHttpExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by alexandremasanes on 29/03/2018.
 */
@RestController
public class ErrorController {

    @RequestMapping("/errors")
    @ResponseStatus(HttpStatus.OK)
    public void onRequest(HttpServletResponse response) {
        if(HttpStatus.valueOf(response.getStatus()).is2xxSuccessful())
            throw new CustomHttpExceptions.ResourceNotFoundException();
    }
}