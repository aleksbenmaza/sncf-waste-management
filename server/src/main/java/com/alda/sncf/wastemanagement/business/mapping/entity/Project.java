package com.alda.sncf.wastemanagement.business.mapping.entity;

import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.Manager;

import lombok.*;

import javax.persistence.*;
import java.util.*;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
@Entity
@Table(
        name              = "projects",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {
                        "due_date",
                        "project_type_id",
                        "company_id"
                }
        )
)
@SequenceGenerator(
        name         = IdentifiedByIdEntity.ID_GENERATOR_NAME,
        sequenceName = "seq__projects"
)
@EqualsAndHashCode(of = {"dueDate", "projectType", "company"})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Project extends IdentifiedByIdEntity {

    @Getter @Setter
    private String description;

    @Getter
    @Column(
            name     = "due_date",
            nullable = false
    )
    private Date dueDate;

    @Getter
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(
            name                 = "project_type_id",
            referencedColumnName = "id"
    )
    private ProjectType projectType;

    @Getter
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(
            name                 = "company_id",
            referencedColumnName = "id"
    )
    private Company company;

    @OneToMany(
            mappedBy      = "project",
            cascade       = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Composition> compositions;

    @Getter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(
            name                 = "manager_id",
            referencedColumnName = "id"
    )
    private Manager manager;

    {
        compositions = new HashSet<>();
    }

    public Project(Date dueDate, ProjectType projectType, Company company) {
        this.dueDate     = dueDate;
        this.projectType = projectType;
        this.company     = company;
        projectType.addProject(this);
        company.addProject(this);
    }


    public Set<Composition> getCompositions() {
        return new HashSet<>(compositions);
    }

    boolean addComposition(Composition composition) {
        return compositions.add(composition);
    }

    public void setManager(Manager manager) {
        if(!manager.getProjects().contains(this))
            manager.addProject(this);

        this.manager = manager;
    }
}