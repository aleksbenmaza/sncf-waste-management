package com.alda.sncf.wastemanagement.web.api.model.output.user;

import com.alda.sncf.wastemanagement.business.mapping.entity.user.Manager;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
public class ManagerDTO extends UserDTO {

    private long[] projectIds;

    public ManagerDTO(Manager manager) {
        super(manager);
        projectIds = getIds(manager.getProjects());
    }
}
