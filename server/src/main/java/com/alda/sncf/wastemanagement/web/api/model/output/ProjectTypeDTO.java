package com.alda.sncf.wastemanagement.web.api.model.output;

import com.alda.sncf.wastemanagement.business.mapping.entity.ProjectType;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
@SuppressWarnings("all")
public class ProjectTypeDTO extends DTO<ProjectType> {

    private String name;

    private String code;

    private long[] projectIds;

    public ProjectTypeDTO(ProjectType entity) {
        super(entity);
        name = entity.getName();
        code = entity.getCode();
        projectIds = getIds(entity.getProjects());
    }
}
