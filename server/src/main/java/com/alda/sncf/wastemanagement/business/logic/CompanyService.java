package com.alda.sncf.wastemanagement.business.logic;


import com.alda.sncf.wastemanagement.business.mapping.entity.Company;

import java.util.List;

/**
 * Created by alexandremasanes on 02/02/2018.
 */
public interface CompanyService {

    interface CompanyCreation {

        String getName();

        String getSirenNumber();
    }

    List<Company> getAll();

    List<Company> get(long... ids);

    Company getOne(long id);

    Company create(CompanyCreation companyCreation);
}