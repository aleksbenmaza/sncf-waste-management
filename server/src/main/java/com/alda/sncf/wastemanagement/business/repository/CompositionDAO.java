package com.alda.sncf.wastemanagement.business.repository;

import com.albema.common.orm.repository.Saver;
import com.alda.sncf.wastemanagement.business.mapping.entity.Composition;

public interface CompositionDAO/* extends Saver<Composition> */{
}
