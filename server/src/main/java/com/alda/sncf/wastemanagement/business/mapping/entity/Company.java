package com.alda.sncf.wastemanagement.business.mapping.entity;

import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.ThirdParty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexandremasanes on 30/01/2018.
 */
@Entity
@Table(name = "companies")
@SequenceGenerator(
        name         = IdentifiedByIdEntity.ID_GENERATOR_NAME,
        sequenceName = "seq__companies"
)
@EqualsAndHashCode(of = "sirenNumber")
public class Company extends IdentifiedByIdEntity {

    @Getter @Setter
    private String name;

    @Getter
    @Column(
            name     = "siren_number",
            nullable = false,
            unique   = true
    )
    private String sirenNumber;

    @OneToMany(
            mappedBy      = "company",
            cascade       = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Project> projects;

    @OneToMany(
            mappedBy      = "company",
            cascade       = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<ThirdParty> thirdParties;

    public Company(String sirenNumber) {
        projects = new HashSet<>();
        thirdParties = new HashSet<>();
        this.sirenNumber = sirenNumber;
    }

    public Set<Project> getProjects() {
        return new HashSet<>(projects);
    }

    public boolean addProject(Project project) {
        return projects.add(project);
    }

    public Set<ThirdParty> getThirdParties() {
        return thirdParties;
    }

    public boolean addThirdParty(ThirdParty thirdParty) {
        return thirdParties.add(thirdParty);
    }

    protected Company() {}
}