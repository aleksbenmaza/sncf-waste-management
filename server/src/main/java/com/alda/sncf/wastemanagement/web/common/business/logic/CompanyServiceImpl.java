package com.alda.sncf.wastemanagement.web.common.business.logic;

import com.alda.sncf.wastemanagement.business.logic.CompanyService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Company;
import com.alda.sncf.wastemanagement.business.repository.CompanyDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
class CompanyServiceImpl implements CompanyService {

    private final CompanyDAO companyDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Company> getAll() {
        return companyDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Company> get(long... ids) {
        return companyDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public Company getOne(long id) {
        return companyDAO.find(id);
    }

    @Override
    @Transactional
    public Company create(CompanyCreation companyCreation) {

        Company company;
        String sirenNumber = companyCreation.getSirenNumber();

        company = new Company(sirenNumber);

        company.setName(companyCreation.getName());

        companyDAO.save(company);

        return company;
    }

    CompanyServiceImpl(CompanyDAO companyDAO) {
        this.companyDAO = companyDAO;
    }
}