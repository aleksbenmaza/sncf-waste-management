package com.alda.sncf.wastemanagement.business.repository;

import com.albema.common.orm.repository.Saver;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;

import java.util.List;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
public interface StockDAO extends Saver<Stock> {

    interface MaterialIdAndQuality {

        long getMaterialId();

        Stock.Quality getQuality();
    }

    List<Stock> find();

    List<Stock> find(MaterialIdAndQuality... materialIdAndQualities);

    List<Stock> find(long materialId);

    List<Stock> find(Stock.Quality quality);

    Stock find(long materialId, Stock.Quality quality);

    boolean remove(long materialId, Stock.Quality quality);

    boolean remove(MaterialIdAndQuality... materialIdAndQualities);
}