package com.alda.sncf.wastemanagement.business.logic;

import com.alda.sncf.wastemanagement.business.mapping.entity.Project;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import com.alda.sncf.wastemanagement.business.repository.StockDAO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Created by alexandremasanes on 02/02/2018.
 */
public interface ProjectService {

    interface StockIdQuantity {

        interface Id extends StockDAO.MaterialIdAndQuality {}

        Id getStockId();

        BigDecimal getQuantity();
    }

    interface StockQuantitiesUpdate {

        StockIdQuantity[] getStockIdQuantities();
    }

    interface ProjectCreation {

        long getProjectTypeId();

        String getDescription();

        Date getDueDate();

        long getThirdPartyId();

        long getMaterialId();

        Stock.Quality getQuality();

        BigDecimal getQuantity();
    }

    List<Project> getAll();

    List<Project> get(long... ids);

    Project getOne(long id);

    Project create(ProjectCreation projectCreation);
}