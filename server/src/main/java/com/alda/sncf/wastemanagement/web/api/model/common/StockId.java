package com.alda.sncf.wastemanagement.web.api.model.common;

import com.albema.spring.validation.ExistingEntity;
import com.alda.sncf.wastemanagement.business.logic.ProjectService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class StockId implements ProjectService.StockIdQuantity.Id, Serializable {

    @Getter
    @ExistingEntity(Material.class)
    private long materialId;

    @Getter
    @NotNull
    private Stock.Quality quality;

    public StockId(long materialId, Stock.Quality quality) {
        this.materialId = materialId;
        this.quality = quality;
    }
}
