package com.alda.sncf.wastemanagement.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.alda.sncf.wastemanagement.business.mapping.entity.Company;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.Project;

import java.math.BigDecimal;
import java.util.List;

public interface ProjectDAO extends Finder<Project>, Remover<Project>, Saver<Project> {

    interface QuantityByMaterialAndCompany {

        Material getMaterial();

        Company getCompany();

        BigDecimal getQuantity();
    }

    List<QuantityByMaterialAndCompany> findQuantitiesByMaterialAndCompany();
}
