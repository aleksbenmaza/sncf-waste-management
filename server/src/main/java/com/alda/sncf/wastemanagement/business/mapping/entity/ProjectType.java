package com.alda.sncf.wastemanagement.business.mapping.entity;

import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexandremasanes on 30/01/2018.
 */
@Entity
@Table(name = "project_types")
@SequenceGenerator(
        name         = IdentifiedByIdEntity.ID_GENERATOR_NAME,
        sequenceName = "seq__project_types"
)
@EqualsAndHashCode(of = "code")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProjectType extends IdentifiedByIdEntity {

    @Getter @Setter
    private String name;

    @Getter
    @Column(unique = true, nullable = false)
    private String code;

    @OneToMany(
            mappedBy      = "projectType",
            cascade       = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Project> projects;

    public ProjectType(String code) {
        this.code = code;
        projects  = new HashSet<>();
    }

    public Set<Project> getProjects() {
        return new HashSet<>(projects);
    }

    boolean addProject(Project project) {
        return projects.add(project);
    }
}