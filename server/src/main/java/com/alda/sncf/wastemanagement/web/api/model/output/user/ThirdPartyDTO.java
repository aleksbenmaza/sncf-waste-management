package com.alda.sncf.wastemanagement.web.api.model.output.user;

import com.alda.sncf.wastemanagement.business.mapping.entity.user.ThirdParty;
import com.alda.sncf.wastemanagement.web.api.model.output.CompanyDTO;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
public class ThirdPartyDTO extends UserDTO {

    @SerializedName("company")
    private CompanyDTO companyDTO;

    public ThirdPartyDTO(ThirdParty entity) {
        super(entity);
        companyDTO = new CompanyDTO(entity.getCompany());
    }
}
