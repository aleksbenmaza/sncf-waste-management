package com.alda.sncf.wastemanagement.web.api.model.input;

import com.alda.sncf.wastemanagement.business.logic.UserService;
import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
public class TokenCreation implements UserService.TokenCreation, Serializable {

    @Getter
    @NotEmpty @Email
    private String emailAddress;

    @Getter
    @NotEmpty
    private String password;
}
