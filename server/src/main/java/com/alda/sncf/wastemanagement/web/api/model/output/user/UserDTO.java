package com.alda.sncf.wastemanagement.web.api.model.output.user;

import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;
import com.alda.sncf.wastemanagement.web.api.model.output.DTO;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
public abstract class UserDTO extends DTO<User> {

    @SerializedName("__TYPE__")
    private String type;

    private String emailAddress;

    private String lastName;

    private String firstName;

    public UserDTO(User entity) {
        super(entity);
        type = entity.getClass().getSimpleName();
        emailAddress = entity.getEmailAddress();
        lastName = entity.getLastName();
        firstName = entity.getFirstName();
    }
}
