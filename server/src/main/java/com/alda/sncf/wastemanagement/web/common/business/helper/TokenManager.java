package com.alda.sncf.wastemanagement.web.common.business.helper;

import com.albema.spring.security.TokenEncryptionManager;
import com.alda.sncf.wastemanagement.business.logic.UserService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Token;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
@Component
public class TokenManager {

    private final StandardPBEStringEncryptor encryptor;

    private final TokenEncryptionManager tokenEncryptionManager;

    private final UserService userService;

    private final int tokenLifetime;

    private final HashMap<String, TokenEncryptionManager.TokenEncryptionComponents> cache;

    {
        cache = new HashMap<>();
    }

    public TokenEncryptionManager.TokenEncryptionComponents toTokenEncryptionComponents(
            Token token
    ) {
        TokenEncryptionManager.TokenEncryptionComponents components;

        components = new TokenEncryptionManager.TokenEncryptionComponents(token.getValue(), token.getUser().getEmailAddress(), token.getCreationTime());

        return components;
    }

    public Token get(String encryptedString) {
        TokenEncryptionManager.TokenEncryptionComponents tokenEncryptionComponents;
        tokenEncryptionComponents = cache.computeIfAbsent(encryptedString, this::fromEncryptedString);
        if(!isValid(tokenEncryptionComponents))
            return null;
        return userService.getOneToken(tokenEncryptionComponents.getUuid());
    }

    public String replaceIfExpired(String encryptedString) {
        Token token;
        Token newToken;

        TokenEncryptionManager.TokenEncryptionComponents tokenEncryptionComponents;

        tokenEncryptionComponents = cache.computeIfAbsent(
                encryptedString,
                this::fromEncryptedString
        );

        if(tokenEncryptionComponents == null)
            return null;

        token = userService.getOneToken(tokenEncryptionComponents.getUuid());

        if(token == null)
            return null;

        if(isExpired(token)) {
            newToken = new Token(token.getUser());
            userService.update(token.getUser());
            tokenEncryptionComponents = new TokenEncryptionManager.TokenEncryptionComponents(
                    newToken.getValue(),
                    tokenEncryptionComponents.getEmailAddress(),
                    token.getCreationTime()
            );
            cache.remove(encryptedString);
            encryptedString = tokenEncryptionManager.toEncryptedString(tokenEncryptionComponents, encryptor::encrypt);
            cache.put(encryptedString, tokenEncryptionComponents);
        }

        return encryptedString;
    }

    public String createEncrypted(User user) {
        Token                      token;
        Token                      newToken;
        String emailAddress;
        TokenEncryptionManager.TokenEncryptionComponents tokenEncryptionComponents;
        String                     encryptedToken;

        if((token = user.getToken()) != null && !isExpired(token)) {
            tokenEncryptionComponents = tokenEncryptionManager.toTokenEncryptionComponents(token.getValue(), token.getUser().getEmailAddress(), token.getCreationTime());
            for (Map.Entry<String, TokenEncryptionManager.TokenEncryptionComponents> entry : cache.entrySet())
                if (entry.getValue().equals(tokenEncryptionComponents))
                    return entry.getKey();
            encryptedToken = tokenEncryptionManager.toEncryptedString(tokenEncryptionComponents, encryptor::encrypt);
            cache.put(encryptedToken, tokenEncryptionComponents);
            return encryptedToken;
        } else {
            emailAddress = user.getEmailAddress();
            newToken = new Token(user);
            userService.update(user);
        }

        tokenEncryptionComponents = new TokenEncryptionManager.TokenEncryptionComponents(
                newToken.getValue(),
                emailAddress,
                newToken.getCreationTime()
        );

        encryptedToken = tokenEncryptionManager.toEncryptedString(tokenEncryptionComponents, encryptor::decrypt);

        cache.put(encryptedToken, tokenEncryptionComponents);

        return encryptedToken;
    }


    public boolean isExpired(TokenEncryptionManager.TokenEncryptionComponents tokenEncryptionComponents) {
        return new Date().compareTo(tokenEncryptionComponents.getTimestamp()) >= tokenLifetime;
    }

    public boolean isExpired(Token token) {
        return new Date().compareTo(token.getCreationTime()) > tokenLifetime;
    }

    public String toEncryptedString(TokenEncryptionManager.TokenEncryptionComponents tokenEncryptionComponents) {
        return tokenEncryptionManager.toEncryptedString(tokenEncryptionComponents, encryptor::encrypt);
    }

    @Scheduled(cron = "0 0 * * * *")
    protected void removeExpiredToken() {
        final Instant currentTime;

        currentTime = Instant.now();

        cache.values().removeIf(
                tokenEncryptionComponents -> currentTime.compareTo(
                        tokenEncryptionComponents.getTimestamp()
                                                 .toInstant()
                ) > tokenLifetime
        );

        userService.removeExpiredTokens();
    }

    TokenManager(
                                       StandardPBEStringEncryptor encryptor,
                                       TokenEncryptionManager     tokenEncryptionManager,
                                       UserService                userService,
            @Value("${tokenLifeTime}") int                        tokenLifetime
    ) {
        this.encryptor = encryptor;
        this.tokenEncryptionManager = tokenEncryptionManager;
        this.userService = userService;
        this.tokenLifetime = tokenLifetime;
    }

    private TokenEncryptionManager.TokenEncryptionComponents fromEncryptedString(String encryptedString) {
        return tokenEncryptionManager.fromEncryptedString(encryptedString, encryptor::decrypt);
    }

    private boolean isValid(TokenEncryptionManager.TokenEncryptionComponents tokenEncryptionComponents) {
        Token token;

        token = userService.getOneToken(tokenEncryptionComponents.getUuid());

        return token != null && !isExpired(token);
    }
}