package com.alda.sncf.wastemanagement.web.api.http.controller.manager;

import com.alda.sncf.wastemanagement.business.logic.MaterialService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.web.api.model.input.MaterialCreation;
import com.alda.sncf.wastemanagement.web.api.model.output.DTO;
import com.alda.sncf.wastemanagement.web.api.model.output.MaterialDTO;

import com.alda.sncf.wastemanagement.web.common.business.helper.TokenManager;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController("managerMaterialController")
@RequestMapping("/m/materials")
public class MaterialController extends Controller {

    private final MaterialService materialService;

    public MaterialController(TokenManager tokenManager, MaterialService materialService) {
        super(tokenManager);
        this.materialService = materialService;
    }

    @GetMapping
    public List<MaterialDTO> onGet() {
        return DTO.fromCollection(materialService.getAll(), MaterialDTO::new);
    }

    @GetMapping(params = "ids")
    public List<MaterialDTO> onGet(@RequestParam long[] ids) {
        return DTO.fromCollection(materialService.get(ids), MaterialDTO::new);
    }

    @GetMapping("/{id}")
    public MaterialDTO onGet(@PathVariable long id) {
        Material material;

        material = materialService.getOne(id);

        return material != null ? new MaterialDTO(material) : null;
    }

    @PostMapping
    public MaterialDTO onPost(@RequestBody @Valid MaterialCreation materialCreation) {
        return new MaterialDTO(materialService.createStock(materialCreation));
    }
}

