package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.ProjectType;
import com.alda.sncf.wastemanagement.business.repository.ProjectTypeDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
class ProjectTypeDAOImpl extends DAO implements ProjectTypeDAO {

    @Override
    public List<ProjectType> find() {
        return find(ProjectType.class);
    }

    @Override
    public List<ProjectType> find(long... ids) {
        return find(ProjectType.class, ids);
    }

    @Override
    public ProjectType find(long id) {
        return find(ProjectType.class, id);
    }

    @Override
    public boolean has(long id) {
        return false;
    }

    @Override
    public long getNextId() {
        return 0;
    }

    @Override
    public boolean remove(long... ids) {
        return remove(ProjectType.class, ids);
    }

    @Override
    public boolean remove(long id) {
        return remove(Material.class, id);
    }

    @Override
    public boolean remove(ProjectType company) {
        return remove0(company);
    }

    @Override
    public boolean remove(ProjectType... company) {
        return remove0(company);
    }

    @Override
    public void save(ProjectType entity) {
        save0(entity);
    }

    protected ProjectTypeDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);

    }
}