package com.alda.sncf.wastemanagement.web.api.model.output;

import com.alda.sncf.wastemanagement.business.mapping.entity.Composition;
import com.alda.sncf.wastemanagement.business.mapping.entity.Project;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import com.alda.sncf.wastemanagement.web.api.model.common.StockId;
import com.alda.sncf.wastemanagement.web.api.model.common.StockIdQuantity;
import com.alda.sncf.wastemanagement.web.api.model.output.user.ManagerDTO;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
@SuppressWarnings("all")
public class ProjectDTO extends DTO<Project> {

    private String description;

    private Date dueDate;

    @SerializedName("projectType")
    private ProjectTypeDTO projectTypeDTO;

    @SerializedName("company")
    private CompanyDTO companyDTO;

    @SerializedName("manager")
    private ManagerDTO managerDTO;

    private StockIdQuantity[] StockIdQuantities;


    public ProjectDTO(Project entity) {
        super(entity);

        int i;

        projectTypeDTO = new ProjectTypeDTO(entity.getProjectType());
        companyDTO     = new CompanyDTO(entity.getCompany());

        if(entity.getManager() != null)
            managerDTO = new ManagerDTO(entity.getManager());

        description = entity.getDescription();
        dueDate = entity.getDueDate();
        StockIdQuantities = new StockIdQuantity[entity.getCompositions().size()];

        i = 0;

        for(Composition composition : entity.getCompositions())
            StockIdQuantities[i++] = new StockIdQuantity(
                    new StockId(
                            composition.getStock().getMaterial().getId(),
                            composition.getStock().getQuality()
                    ),
                    composition.getQuantity()
            );
    }
}