package com.alda.sncf.wastemanagement.business.mapping.entity.user;

import com.alda.sncf.wastemanagement.business.mapping.entity.Project;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexandremasanes on 30/01/2018.
 */
@Entity
@Table(name = "managers")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Manager extends User {

    @OneToMany(
            mappedBy      = "manager",
            orphanRemoval = true,
            cascade       = CascadeType.ALL
    )
    private Set<Project> projects;

    public Manager(String emailAddress) {
        super(emailAddress);
    }

    public Set<Project> getProjects() {
        return new HashSet<>(projects);
    }

    public boolean addProject(Project project) {
        if(!equals(project.getManager())) {
            projects.add(project);
            project.setManager(this);
            return true;
        } else
            return false;
    }
}