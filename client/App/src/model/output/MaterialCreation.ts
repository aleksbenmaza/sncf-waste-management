/**
 * Created by alexandremasanes on 29/03/2018.
 */

export interface MaterialCreation {

  name: string;

  quantity: number;

  recyclable: boolean;
}
