/**
 * Created by alexandremasanes on 29/03/2018.
 */

export interface ProjectCreation {

  projectTypeId: number;

  thirdPartyId: number;

  description: string;

  dueDate: Date;
}
