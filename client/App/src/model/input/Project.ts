/**
 * Created by alexandremasanes on 29/03/2018.
 */

import {Quality, StockId} from "./Stock";
import {ProjectType} from "./ProjectType";
import {ThirdParty} from "./user/ThirdParty";
import {Manager} from "./user/Manager";

export interface Project {

  id: number;

  projectType: ProjectType;

  thirdParty: ThirdParty;

  manager: Manager;

  dueDate: Date;

  name: string;

  stockIdQuantities: [{stockId :StockId, quantity: number}];
}
