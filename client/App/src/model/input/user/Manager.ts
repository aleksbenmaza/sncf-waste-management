import {User} from "./User";
/**
 * Created by alexandremasanes on 04/02/2018.
 */
export interface Manager extends User {

  projectIds: number[];
}
