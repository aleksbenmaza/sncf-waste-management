/**
 * Created by alexandremasanes on 04/02/2018.
 */
export interface Company {

  id: number;

  sirenNumber: string;

  name: string;

  projectIds: number[];

  thirdPartyIds: number[];
}
