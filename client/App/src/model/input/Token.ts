import {User} from "./user/User";
/**
 * Created by alexandremasanes on 04/02/2018.
 */
export interface Token {

  id: number;

  value: string;

  user: User;
}
