import {Component, OnInit, Input, EventEmitter, OnDestroy, Output} from '@angular/core';
import {TokenCreation} from "../../../../model/output/TokenCreation";
import {TokenService} from "../../../service/token.service";
import {Token} from "../../../../model/input/Token";
import {Title} from "@angular/platform-browser";

const PAGE_TITLE: string = 'Login';

@Component({
  selector: 'login-form',
  templateUrl: 'login-form.component.html',
  styleUrls: ['login-form.component.css'],
  providers: [
    TokenService
  ]
})
export class LoginFormComponent implements OnInit, OnDestroy {

  @Input()
  loginForm: TokenCreation = {
    emailAddress: null,
    password: null
  };

  @Input()
  rembemberMe: boolean = false;

  @Output()
  authenticationChange: EventEmitter<Token> = new EventEmitter;

  constructor(private tokenService: TokenService, title: Title) {
    title.setTitle(PAGE_TITLE);
  }

  public ngOnInit(): void {

  }

  public signIn(): void {

    let self;

    self = this;
    console.log("toto");
    this.tokenService.postToken(this.loginForm)
                     .subscribe(
                       (token: Token) => self.authenticationChange.emit(token),
                       (error) => console.log(error)
                     );
  }

  public ngOnDestroy(): void {
    this.authenticationChange.unsubscribe();
  }
}
