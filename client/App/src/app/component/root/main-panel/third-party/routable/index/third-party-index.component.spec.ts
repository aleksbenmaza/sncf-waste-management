import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import ThirdPartyIndexComponent from './third-party-index.component';

describe('ThirdPartyIndexComponent', () => {
  let component: ThirdPartyIndexComponent;
  let fixture: ComponentFixture<ThirdPartyIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdPartyIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartyIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createStock', () => {
    expect(component).toBeTruthy();
  });
});
