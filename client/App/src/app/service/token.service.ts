import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TokenCreation} from "../../model/output/TokenCreation";
import {Token} from "../../model/input/Token";
import {Observable} from "rxjs";
import {API_SERVER, PUBLIC_API_SUBDOMAIN} from "./globals";

const RESOURCE_URI: string = 'http://' + API_SERVER + '/' + PUBLIC_API_SUBDOMAIN + '/tokens';

@Injectable()
export class TokenService {

  constructor(private httpClient: HttpClient) { }

  public postToken(tokenCreation: TokenCreation): Observable<Token> {
    return this.httpClient.post<Token>(RESOURCE_URI, tokenCreation);
  }
}
