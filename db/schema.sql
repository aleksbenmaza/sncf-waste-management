-----------------
--- SEQUENCES ---
-----------------

CREATE SEQUENCE seq__materials;

CREATE SEQUENCE seq__project_types;

CREATE SEQUENCE seq__projects;

CREATE SEQUENCE seq__companies;

CREATE SEQUENCE seq__users;

---------------
--- DOMAINS ---
---------------

CREATE DOMAIN quality SMALLINT CHECK(VALUE BETWEEN 0 AND 2);

--------------
--- TABLES ---
--------------

CREATE TABLE materials (
  id INTEGER PRIMARY KEY DEFAULT NEXTVAL('seq__materials'),
  code CHAR(8) NOT NULL UNIQUE,
  name VARCHAR,
  recyclable BOOLEAN
);

CREATE TABLE stocks (
  id INTEGER NOT NULL REFERENCES materials ON DELETE CASCADE,
  quality quality NOT NULL,
  quantity DECIMAL,
  PRIMARY KEY (id, quality)
);

CREATE TABLE project_types (
  id INTEGER PRIMARY KEY DEFAULT NEXTVAL('seq__project_types'),
  code CHAR(8) NOT NULL UNIQUE,
  name VARCHAR
);

CREATE TABLE companies (
  id INTEGER PRIMARY KEY DEFAULT NEXTVAL('seq__companies'),
  siren_number CHAR(9) UNIQUE NOT NULL,
  name VARCHAR
);

CREATE TABLE users (
  id INTEGER NOT NULL UNIQUE,
  email_address VARCHAR UNIQUE,
  hash CHAR(64),
  last_name VARCHAR,
  first_name VARCHAR
);

CREATE TABLE managers (
  id INTEGER PRIMARY KEY DEFAULT NEXTVAL('seq__users')
) INHERITS(users);

CREATE TABLE third_parties (
  id INTEGER PRIMARY KEY DEFAULT NEXTVAL('seq__users'),
  company_id INTEGER NOT NULL REFERENCES companies ON DELETE CASCADE
) INHERITS(users);

CREATE TABLE projects (
  id INTEGER PRIMARY KEY DEFAULT NEXTVAL('seq__projects'),
  project_type_id INTEGER NOT NULL REFERENCES project_types ON DELETE CASCADE,
  company_id INTEGER NOT NULL REFERENCES companies ON DELETE CASCADE,
  manager_id INTEGER REFERENCES managers ON DELETE SET NULL,
  due_date DATE NOT NULL,
  description VARCHAR,
  name VARCHAR,
  UNIQUE(project_type_id, company_id, due_date)
);

CREATE TABLE tokens (
  id INTEGER PRIMARY KEY,
  value UUID NOT NULL UNIQUE,
  creation_time TIMESTAMP DEFAULT NOW()
);

CREATE TABLE stocks_projects (
  material_id INTEGER NOT NULL,
  quality quality NOT NULL,
  project_id INTEGER NOT NULL REFERENCES projects ON DELETE CASCADE,
  quantity DECIMAL,
  PRIMARY KEY(material_id, quality, project_id),
  FOREIGN KEY(material_id, quality) REFERENCES stocks ON DELETE CASCADE
);

-------------
--- VIEWS ---
-------------

CREATE VIEW materials_used_quantities AS
  SELECT material_id AS id, SUM(quantity) AS quantity
  FROM projects JOIN stocks_projects ON id = project_id
  WHERE due_date < NOW()
  GROUP BY material_id
  ORDER BY 1, 2;

CREATE VIEW materials_used_quantities_years AS
  SELECT material_id AS id, EXTRACT(YEAR FROM due_date) AS year, SUM(quantity) AS quantity
  FROM projects JOIN stocks_projects ON id = project_id
  WHERE due_date < NOW()
  GROUP BY material_id, EXTRACT(YEAR FROM due_date)
  ORDER BY 1, 2 DESC, 3;

CREATE VIEW stocks_used_quantities AS
  SELECT material_id AS id, sp.quality as quality, SUM(sp.quantity) AS quantity
  FROM stocks s
    JOIN stocks_projects sp
      ON s.id = sp.material_id AND s.quality = sp.quality
    JOIN projects p
      ON p.id = project_id
  WHERE due_date < NOW()
  GROUP BY material_id, sp.quality
  ORDER BY 1, 2;

CREATE VIEW void AS
  SELECT NULL;

-----------------
--- FUNCTIONS ---
-----------------

CREATE OR REPLACE FUNCTION delete_token() RETURNS TRIGGER AS $$
BEGIN
  DELETE FROM tokens WHERE id=OLD.id;
  RETURN OLD;
END $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_quantity() RETURNS TRIGGER AS $$
  DECLARE v_quantity DECIMAL DEFAULT 0.0;
BEGIN
  SELECT quantity INTO v_quantity FROM stocks WHERE id=NEW.material_id AND quality=NEW.quality;
  IF NEW.quantity > v_quantity THEN
    RAISE EXCEPTION '`stocks_projects.quantity` can not be more than `stocks.quantity`';
  END IF;

  RETURN NEW;
END $$ LANGUAGE plpgsql;

----------------
--- TRIGGERS ---
----------------

CREATE TRIGGER on_delete__users
AFTER DELETE ON users
FOR EACH ROW
EXECUTE PROCEDURE delete_token();

CREATE TRIGGER on_insert__stocks_projects
BEFORE INSERT ON stocks_projects
FOR EACH ROW
EXECUTE PROCEDURE check_quantity();

--------------
--- INSERT ---
--------------

INSERT INTO materials VALUES(DEFAULT, 'FE', 'Fer', true),
  (DEFAULT, 'CU', 'Cuivre', true),
  (DEFAULT, 'BO', 'Bois', true);

INSERT INTO stocks
VALUES
  (1, 0, 5505.305),
  (2, 0, 12024.50),
  (3, 1, 3555.30),
  (1, 1, 6035.530),
  (2, 1, 395303.30),
  (3, 2, 9359.30);

INSERT INTO project_types VALUES(DEFAULT, 'PV', 'Privé'), (DEFAULT, 'PB', 'Public');

INSERT INTO companies VALUES(DEFAULT, '300VNX03', 'Veolia'), (DEFAULT, '395CB025', 'Toto');

INSERT INTO managers VALUES(DEFAULT, 'manager@waste-management.com', 'e434c2083b2ae7eea564e18e1afd7a6439b3e14867793d2420c49d0ae70d10c6', 'Durand', 'Fernand');

INSERT INTO third_parties VALUES(DEFAULT, 'toto@toto.com', 'b7f9e5e30520d85cd5869fa682c2c2d0de9c6f174269334792a4c7359d9fd314', 'Toto', 'Toto', 1);

INSERT INTO projects
VALUES
  (DEFAULT, 1, 1, NULL, '2018-03-27', 'ptdr', 'toto'),
  (DEFAULT, 2, 1, NULL, '2015-01-12', 'azda', 'aah'),
  (DEFAULT, 2, 1, NULL, '2019-11-22', 'azda', 'aah'),
  (DEFAULT, 2, 2, NULL, '2018-11-22', 'azda', 'hah');

INSERT INTO stocks_projects
VALUES
  (1, 0, 1, 535.299999999999955),
  (2, 0, 2, 2750.350000000000023),
  (3, 1, 2, 575.350000000000023),
  (1, 1, 3, 1750.350000000000023),
  (2, 1, 3, 755.350000000000023),
  (3, 2, 2, 553.350000000000023),
  (1, 1, 1, 856.350000000000023),
  (3, 2, 3, 1550.350000000000023),
  (3, 2, 1, 275.350000000000023);